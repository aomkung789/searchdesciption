const dataService = {
  dataList: [
    {
      id: 1,
      position: 'Finance & Accounting Supervisor',
      company: 'บริษัท เอเซียเสริมกิจลีสซิ่ง จำกัด (มหาชน)',
      responsibility:
        'Perform daily accounting routines such as handling invoices, payments, receipts and posting',
      characteristic: 'Age around 28 years.',
      type_of_work: 'งานประจำ',
      education: 'ปริญญาตรี',
      amount: '1',
      gender: 'ชาย/หญิง',
      salary: 'ขึ้นอยู่กับคุณสมบัติและประสบการณ์',
      experience: '1-8 ปี',
      place: 'กทม. (สาทร)',
      security: 'การฝึกอบรมและพัฒนาพนักงาน',
      contactname: 'Pornpim Samasiri (K.Poy)',
      address:
        'Human Resources Department 24th Fl., Sathorn City Tower, 175 South Sathorn Rd., Tungmahamek, Sathorn, Bangkok 10120',
      phone: '081-8908417, 02-6796262 (1419)',
      homepage: 'http://www.ask.co.th'

    }
  ],
  form: {
    id: -1,
    position: '',
    company: '',
    responsibility: '',
    characteristic: '',
    type_of_work: '',
    education: '',
    amount: '',
    gender: '',
    salary: '',
    experience: '',
    place: '',
    security: '',
    contactname: '',
    address: '',
    phone: '',
    homepage: ''
  },
  lastId: 2,
  getData () {
    return [...this.dataList]
  },
  getForm () {
    return [...this.form]
  }
}
export default dataService
